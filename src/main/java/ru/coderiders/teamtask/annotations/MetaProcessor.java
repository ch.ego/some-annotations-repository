package ru.coderiders.teamtask.annotations;

import ru.coderiders.teamtask.threads.TagProvider;

import java.lang.reflect.Field;

public class MetaProcessor {
    public static void main(String[] args) throws IllegalAccessException {
        Class<TagProvider> clazz = TagProvider.class;
        for (Field field: clazz.getFields()){
            if (field.isAnnotationPresent(MusicMeta.class)){
                if (field.equals(null)){
                    System.out.println(field.getName() + "is empty");
                }
                field.set("", field.getAnnotation(MusicMeta.class).name());
            }
        }
    }
}
